import * as api from '../api';

const fetchInitTodos = todos => ({
    type: "FETCH_INIT_TODOS",
    payload: todos
});

const addTodo = todo => ({
    type: "ADD_TODO",
    payload: todo
});

const removeTodo = id => ({
    type: "REMOVE_TODO",
    payload: id
});

const updateTodo = updatedTodo => ({
    type: "UPDATE_TODO",
    payload: updatedTodo
});

const showError = err => {
    alert(err.message);
};

//Async actions
export const fetchTodosAsync = () => dispatch => {
    return api.getAllTodos()
        .then(todos => dispatch(fetchInitTodos(todos)))
        .catch(err => showError(err));
}

export const addTodoAsync = text => dispatch => {
    return api.addTodo(text)
        .then(todo => dispatch(addTodo(todo)))
        .catch(err => showError(err));
}

export const removeTodoAsync = id => dispatch => {
    return api.removeTodo(id)
        .then(() => dispatch(removeTodo(id)))
        .catch(err => showError(err));
}

export const completeTodoAsync = id => dispatch => {
    return api.changeCompleteStatus(id, true)
        .then(todo => dispatch(updateTodo(todo)))
        .catch(err => showError(err));
}

export const incompleteTodoAsync = id => dispatch => {
    return api.changeCompleteStatus(id, false)
        .then(todo => dispatch(updateTodo(todo)))
        .catch(err => showError(err));
}

export const updateTodoTextAsync = (id, text) => dispatch => {
    return api.updateTodoText(id, text)
        .then(todo => dispatch(updateTodo(todo)))
        .catch(err => showError(err));
}