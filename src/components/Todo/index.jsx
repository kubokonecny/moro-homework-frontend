import React, { Component } from 'react';
import { connect } from 'react-redux';
import { todoActions } from '../../actions';
import styles from './todo.scss';

const CheckBox = ({ checked, ...rest }) => (
    <div className={styles.checkbox} {...rest}>
        {checked && <span className={styles.checkboxMark}></span>}
    </div>
);

class Todo extends Component {
    defaultProps = {

    }
    
    state = {
        isRenaming: false
    };

    handleToggleCheckbox(e) {
        if (this.props.completed) {
            this.props.incompleteTodo(this.props.id);
        } else {
            this.props.completeTodo(this.props.id);
        }
    }

    handleRenameTask() {
        this.setState({
            isRenaming: true
        })
    }

    handleOnBlur(e) {
        this.handleUpdateTask(e.target.value);
        this.setState({
            isRenaming: false
        });
    }

    handleKeyPress(e) {
        if (e.key === 'Enter') {
            this.handleUpdateTask(e.target.value);
        }
    }

    handleUpdateTask(text) {
        this.props.updateTodoText(this.props.id, text);
        this.setState({
            isRenaming: false
        });
    }

    handleRemoveTodo() {
        this.props.removeTodo(this.props.id);
    }

    render() {
        return (
            <li className={[styles.todo, this.props.completed && styles.todoCompleted].join(" ")}>
                <CheckBox checked={this.props.completed} onClick={this.handleToggleCheckbox.bind(this)} />
                {this.state.isRenaming ? (
                    <input type="text"
                        spellCheck="false"
                        ref={el => this.renameInput = el}
                        defaultValue={this.props.text}
                        onBlur={this.handleOnBlur.bind(this)}
                        onKeyPress={this.handleKeyPress.bind(this)}
                    />) : (
                        <span className={this.props.completed && styles.todoTextCompleted}
                            onDoubleClick={this.handleRenameTask.bind(this)}>
                            {this.props.text}
                        </span>
                    )}
                <i className="material-icons"
                    onClick={this.handleRemoveTodo.bind(this)}>
                    clear
                </i>
            </li>
        );
    }
}

const mapDispatchToProps = dispatch => ({
    removeTodo: id => dispatch(todoActions.removeTodoAsync(id)),
    completeTodo: id => dispatch(todoActions.completeTodoAsync(id)),
    incompleteTodo: id => dispatch(todoActions.incompleteTodoAsync(id)),
    updateTodoText: (id, text) => dispatch(todoActions.updateTodoTextAsync(id, text))
});

export default connect(false, mapDispatchToProps)(Todo);