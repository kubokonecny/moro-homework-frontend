import React, { Component } from 'react';
import TodoInput from '../TodoInput';
import TodoList from '../TodoList';
import styles from './container.scss';

export default class Container extends Component {

    render() {
        return (
            <div className={styles.container}>
                <div className={styles.heading}> 
                    <h1 className={styles.mainHeading}>TODO<span>LIST</span></h1>
                    <p className={styles.subHeading}>Example designed for MoroSystems</p>
                </div>
                <TodoInput/>
                <TodoList/>
            </div>
        )
    }
}

