import React, { Component } from 'react';
import { connect } from 'react-redux';
import { todoActions } from '../../actions';
import { visibilityTypes, applyFilter } from './visibilityFilter';
import Todo from '../Todo';
import * as api from '../../api';
import styles from './todolist.scss';

const filterItems = [
    { name: "All", type: visibilityTypes.SHOW_ALL },
    { name: "Completed", type: visibilityTypes.SHOW_COMPLETED },
    { name: "Incompleted", type: visibilityTypes.SHOW_INCOMPLETED }
];

class TodoList extends Component {
    state = {
        visibilityType: visibilityTypes.SHOW_ALL
    }

    componentDidMount() {
        this.props.fetchTodos();
    }

    handleVisibilityChange(type) {
        this.setState({
            visibilityType: type
        });
    }

    handleMarkAllComplete() {
        applyFilter(this.props.todos, this.state.visibilityType).forEach(todo => {
            if (!todo.completed) {
                this.props.completeTodo(todo.id);
            }
        });
    }

    handleRemoveAllCompleted() {
        this.props.todos.forEach(todo => {
            if (todo.completed) {
                this.props.removeTodo(todo.id);
            }
        });
    }

    render() {
        return (
            <ul className={styles.taskList}>
                {applyFilter(this.props.todos, this.state.visibilityType).map(todo => (
                    <Todo
                        key={todo.id}
                        id={todo.id}
                        completed={todo.completed}
                        text={todo.text} />
                ))}
                <li className={styles.todoFilter}>
                    <p>Completed:&nbsp;
                        <span>
                            {applyFilter(this.props.todos, visibilityTypes.SHOW_COMPLETED).length}
                        </span>
                    </p>
                    <ul>
                        {filterItems.map(item => (
                            <li key={item.name}
                                className={this.state.visibilityType === item.type && styles.filterActive}
                                onClick={() => this.handleVisibilityChange(item.type)}>
                                {item.name}
                            </li>
                        ))}

                    </ul>
                </li>
                <li className={styles.controlButtons}>
                    <button onClick={this.handleMarkAllComplete.bind(this)}>Mark all completed</button>
                    <button onClick={this.handleRemoveAllCompleted.bind(this)}>Remove completed</button>
                </li>
            </ul>
        )
    }
}

const mapStateToProps = state => ({
    todos: state.todos
});

const mapDispatchToProps = dispatch => ({
    fetchTodos: () => dispatch(todoActions.fetchTodosAsync()),
    completeTodo: id => dispatch(todoActions.completeTodoAsync(id)),
    removeTodo: id => dispatch(todoActions.removeTodoAsync(id))
});

export default connect(mapStateToProps, mapDispatchToProps)(TodoList);