const visibilityTypes = {
    SHOW_ALL: "SHOW_ALL",
    SHOW_COMPLETED: "SHOW_COMPLETED",
    SHOW_INCOMPLETED: "SHOW_INCOMPLETED"
}

const applyFilter = (todos, visibilityType) => {
    switch (visibilityType) {
        case visibilityTypes.SHOW_ALL:
            return todos;

        case visibilityTypes.SHOW_COMPLETED:
            return todos.filter(todo => todo.completed === true);

        case visibilityTypes.SHOW_INCOMPLETED:
            return todos.filter(todo => todo.completed === false);

        default:
            return todos;
    }
}

export {visibilityTypes, applyFilter};