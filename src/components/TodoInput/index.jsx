import React, { Component } from 'react';
import { todoActions } from '../../actions';
import { connect } from 'react-redux';
import styles from './todoinput.scss';
import rightArrow from '../../images/right_arrow.png';

class TodoInput extends Component {

    handleKeyPress(e) {
        if (e.key === 'Enter') {
            this.props.addTodo(e.target.value);
            e.target.value = "";
        }
    }

    handleClick() {
        this.props.addTodo(this.inputTodo.value);
        this.inputTodo.value = "";
    }

    render() {
        return (
            <label className={styles.taskInputLabel}>
                <img className={styles.arrowIcon} 
                    src={rightArrow}
                    onClick={this.handleClick.bind(this)} />
                <input type="text" 
                    className={styles.taskInput}
                    spellCheck="false"
                    placeholder="Add new task"
                    ref={el => this.inputTodo = el}
                    onKeyPress={this.handleKeyPress.bind(this)} />
            </label>
        )
    }
}

const mapDispatchToProps = dispatch => ({
    addTodo: text => dispatch(todoActions.addTodoAsync(text))
});

export default connect(false, mapDispatchToProps)(TodoInput);