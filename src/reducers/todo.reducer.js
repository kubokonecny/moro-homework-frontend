const todos = [];

const filterTodoFromArray = (id, state) => {
    return state.filter(todo => todo.id !== id);
}
const updateTodoArray = (updatedTodo, state) => {
    return state.map(todo => {
        return (todo.id === updatedTodo.id) ? updatedTodo : todo;
    })
}

const todoReducer = (state = todos, action) => {
    switch (action.type) {
        case "FETCH_INIT_TODOS":
            return [
                ...state,
                ...action.payload
            ]

        case "ADD_TODO":
            return [
                ...state,
                action.payload
            ];

        case "REMOVE_TODO":
            return filterTodoFromArray(action.payload, state);

        case "UPDATE_TODO":
            return updateTodoArray(action.payload, state);

        default:
            return state;
    }
}

export default todoReducer;