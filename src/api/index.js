import axios from 'axios';

const axiosInstance = axios.create({
    baseURL: "http://localhost:8080/",
    timeout: 2000,
});

export const getAllTodos = () => new Promise((resolve, reject) => {
    axiosInstance.get("/todos")
        .then(res => resolve(res.data))
        .catch(err => reject(err));
});

export const addTodo = text => new Promise((resolve, reject) => {
    axiosInstance.post("/todos", { text })
        .then(res => resolve(res.data))
        .catch(err => reject(err));
});

export const removeTodo = id => new Promise((resolve, reject) => {
    axiosInstance.delete(`/todos/${id}`)
        .then(() => resolve())
        .catch(err => reject(err));
});

export const changeCompleteStatus = (id, completeStatus) => new Promise((resolve, reject) => {
    const status = completeStatus ? "complete" : "incomplete";
    axiosInstance.post(`/todos/${id}/${status}`)
        .then(res => resolve(res.data))
        .catch(err => reject(err));
});

export const updateTodoText = (id, text) => new Promise((resolve, reject) => {
    axiosInstance.post(`/todos/${id}`, { text })
        .then(res => resolve(res.data))
        .catch(err => reject(err));
});