import React, { Component } from 'react';
import {Provider} from 'react-redux';
import Container from './components/Container';
import store from './store';
//Import globals and base
import './scss/app';

export default class App extends Component {
  
  render() {
    return (
      <Provider store={store}>
        <Container/>
      </Provider>
    );
  }
  
}