import { createStore, combineReducers, applyMiddleware} from 'redux';
import thunk from 'redux-thunk';
import todos from './reducers/todo.reducer';

const reducer = combineReducers({todos});
const store = createStore(reducer,applyMiddleware(thunk));
//For debugging only
window.store = store;

export default store;